package cn.stylefeng.roses.kernel.dict.modular.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 基础字典
 * </p>
 *
 * @author fengshuonan
 * @since 2018-07-25
 */
@Data
@TableName("cbd_sys_dict")
public class Dict implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 字典id
     */
    @TableId("dict_id")
    @ApiModelProperty("字典id")
    private String dictId;

    /**
     * 字典类型id
     */
    @TableField("dict_type_code")
    @ApiModelProperty("字典类型id")
    private String dictTypeCode;

    /**
     * 字典编码
     */
    @TableField("dict_code")
    @ApiModelProperty("字典编码")
    private String dictCode;

    /**
     * 字典名称
     */
    @TableField("dict_name")
    @ApiModelProperty("字典名称")
    private String dictName;

    /**
     * 简称
     */
    @TableField("dict_short_name")
    @ApiModelProperty("简称")
    private String dictShortName;

    /**
     * 字典简拼
     */
    @TableField("dict_short_code")
    @ApiModelProperty("字典简拼")
    private String dictShortCode;

    /**
     * 上级代码id
     */
    @TableField("parent_id")
    @ApiModelProperty("上级代码id")
    private String parentId;

    /**
     * 状态(1:启用,2:禁用)
     */
    @TableField("status")
    @ApiModelProperty("状态(1:启用,2:禁用)")
    private Integer status;

    /**
     * 创建时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
     * 创建人
     */
    @TableField(value = "create_user", fill = FieldFill.INSERT)
    @ApiModelProperty("创建人")
    private Long createUser;

    /**
     * 更新时间
     */
    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    @ApiModelProperty("更新时间")
    private Date updateTime;

    /**
     * 更新人
     */
    @TableField(value = "update_user", fill = FieldFill.UPDATE)
    @ApiModelProperty("更新人")
    private Long updateUser;

    /**
     * 删除标记
     */
    @TableField("del_flag")
    @ApiModelProperty("删除标记")
    private String delFlag;

    /**
     * 排序
     */
    @TableField(value = "dict_sort")
    @ApiModelProperty("排序")
    private Double dictSort;

}
